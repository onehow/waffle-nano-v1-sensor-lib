# 时钟

## 案例展示

一个能显示时间，日期和星期的时钟。

![](img/IMG20210717202251.jpg)

## 物理连线

### 传感器选择

传感器选择如下图所示的型号为`DS1307`的时钟模组。

![](img/IMG20210713195601.jpg)

### 传感器接线

传感器与`Waffle Nano`之间的接线方式如下表所示，且未在下表中显示的引脚均处于悬空不连状态。

| Waffle Nano | 传感器 |
| ----------- | ------ |
| 3V3         | VCC    |
| IO0         | SDA    |
| IO1         | SCL    |
| GND         | GND    |

（注：上述传感器接线柱均为五接线柱一侧，即上图下方的五个接线柱）

## 传感器库使用

可以获取[ds1307.py](https://gitee.com/blackwalnutlabs/waffle-nano-v1-sensor-lib/blob/master/DS1307/code/ds1307.py)，将此库通过[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor)的文件上传功能将此库上传到`Waffle Nano`上。

我们可以在主函数中使用以下代码导入此库。

```python
import ds1307
```

在对象构造函数中，我们需要传入一个已经构造好的`I2C`对象

```python
# 此处省略I2C对象的构造过程
ds = ds1307.DS1307(i2c)    # 构造时钟对象
```

导入此库后，我们可以进行以下操作

```python
datetime()         datetime(now)         halt(False)
```

1.`ds.datetime()`

该函数能读出`DS1307`现在的时间，所以，你可以使用`print(ds.datetime())`来显示时间（注：若未的设置时间，只能读出其内置的初始值，即`（2000,1,1,0,0,0,0）`）。

2.`ds.datetime(now)`

该函数用于设置时间，具体设置方法如下。

```python
now=(2021,7,1,4,0,0,0)
ds.datetime(now)
```

3.`ds.halt(False)`

该函数用于控制振荡器，`False`激活，`True`关闭。

关于此库相关细节说明详见代码注释。

## 案例代码复现

可以获取[main.py](https://gitee.com/blackwalnutlabs/waffle-nano-v1-sensor-lib/blob/master/DS1307/code/main.py)代码将其内容复制到[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor)编辑器上传输给`Waffle Nano`，以复现此案例，基础效果如文档开始图片所示。

案例相关细节说明详见代码注释。

