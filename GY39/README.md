# 体感温度测试计

## 案例展示

&emsp;&emsp;收集当前环境下的温度，湿度，压强，光照强度以及海拔高度，显示在屏幕上，并且可以通过MQTT协议进行询问和操作。

![](img/All.jpg)

&emsp;&emsp;图为全部同时显示所有数据的样例展示。

## 物理连接

### 传感器选择

&emsp;&emsp; 传感器选择如下图所示的型号为GY-39的温湿度传感器模组。

![](img/gy39.jpg)

### 传感器接线

&emsp;&emsp;传感器与Waffle Nano 之间的接线方式如下表所示，且未在下表中显示的引脚均处于悬空不连装态。

|Waffle Nano|传感器|
|---|---|
|3.3|VCC|
|IO0|DR|
|IO5| CT     |
|GND|GND|


## 传感器库使用

&emsp;&emsp;可以获取[GY39.py](/code/GY39.py),将此库通过[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 的文件上传功能将此库上传到`Waffle Nano`上。

&emsp;&emsp;我们在可以在主函数中使用以下代码导入此库。

```python
import GY39 
```

&emsp;&emsp;直接使用其中函数进行屏幕初始化、网络连接和构造对象。

```python
display=GY39.Display(Prism)
#初始化屏幕
GY39.Connect_Wifi(Username,Password)
#接入WiFi
gy39=GY39.Gy39(display,Mode,Size,Prism,Client_id,Mqtt_server,Topic,mqtt_user,mqtt_password)
#构造对象，因为导入参数多，在main程序中有各参数初始化
try:
    gy39.connect_and_subscribe()
except OSError as e:
    gy39.restart_and_reconnect()
#接入MQTT
```

&emsp;&emsp;使用Break进行判断是否结束程序，如果不结束，则不断读取并执行指令。

```python
while True:
    Break=gy39.sub_and_pub()
    if Break==1:
        break   
```
&emsp;&emsp;关于此库相关细节说明详见代码注释

## 案例代码复现

&emsp;&emsp;可以获取[main.py](/code/main.py)代码，将其内容复制到[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 编辑器上传输给`Waffle Nano`，以复现此案例，基础效果如下所示：

![](img/Prism.jpg)


&emsp;&emsp;案例相关细节说明详见代码注释
