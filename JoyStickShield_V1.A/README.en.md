# Mini game 2048, based on Waffle Nano

&emsp;&emsp;Due to my poor English, this translated document is only for reference, for more precised information, you may check out the original Chinese version.

## Case review

### Introduction of the main function

&emsp;&emsp;Remake the classic mini game 2048, making it possible to be played on Waffle Nano using JoyStick Shield V1.A module.

![](img/inGame.jpg)

&emsp;&emsp;Picture of an in-game view.

### About the rule of the game

&emsp;&emsp;There are some numbers put into a 4×4 matrix, with every number taking up a cell. When you give a command among up, down, left and right, these numbers will move accordingly until they are blocked. Meanwhile, if two same numbers are neighbours in this direction, they will be added together and you can get a new number. Accordingly, the cells occupied will decrease. Each time you move, there will generate a new number in a random available cell. When all the cells are taken up without numbers that can be added together, the game is over. The game score is the sum of all numbers came from adding.

### How to play

&emsp;&emsp;To firstly enter the game, press button B.

&emsp;&emsp;Each time the game starts, you may push the joystick to any place to create a random game according to the instruction. 

&emsp;&emsp;Use the joystick to move up, down, left or right.

&emsp;&emsp;When the game cannot continue, the game is over. Press button C to go back and start a new game.

## Physical connection

### Selection of sensors

&emsp;&emsp; Choose JoyStick Shield module as shown below.

![](img/joystickShield.jpg)

### Wiring of the sensor

&emsp;&emsp;JoyStick Shield module does NOT have pin names on it. The pins actually used are shown below. Between "joystick pins", the pin of X-axis is on the left, while the other one is for Y-axis.

![](img/joystickOutput_en.jpg)

&emsp;&emsp;Wiring between the sensor and Waffle Nano is shown below. 

![](img/connect_en.png)

## How to use sensor library

&emsp;&emsp;You can get [joystickShield.py](/code/joystickShield.py), use the file upload function of [Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) to upload the library to `Waffle Nano`.

&emsp;&emsp;We can use the following code in the main function to import this library.

```python
from joystickShield import Cmd 
```

&emsp;&emsp;To use the library, we need to define a `Cmd` object.

```python
command=Cmd()
```

&emsp;&emsp;Using `statusA()` of a `Cmd` object, we can get the status of button A on the JoyStick Shield module - 0 is for being pressed, while 1 is for not being pressed. This is also capable for button B, C and D.

```python
a=command.statusA()
b=command.statusB()
c=command.statusC()
d=command.statusD()
```

&emsp;&emsp;Using `statusX()` of a `Cmd` object, we can get the status of X-axis of the joystick - take the centre of the joystick as the origin, -1 is for the negative direction, 0 is for not moving, and 1 is for the positive direction. This is also capable for Y-axis.

&emsp;&emsp;For X-axis, right is the positive direction.

&emsp;&emsp;For Y-axis, up is the positive direction.

```python
x=command.statusX()
y=command.statusY()
```

&emsp;&emsp;Using `exactX()` of a `Cmd` object, we can get the exact position of the joystick in X-axis - a value. This is also capable for Y-axis.

```python
x=command.exactX()
y=command.exactY()
```

&emsp;&emsp;For relevant details of this library, you can also check out the code comments.

## Case reproduction

&emsp;&emsp;You can get [main.py](/code/main.py), and copy the content to code editor [Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) and upload to `Waffle Nano`, to reproduce the case. Basic effect is shown below.

![](img/start.jpg)


&emsp;&emsp;For relevant details of this case, you can also check out the code comments.

&emsp;&emsp;You can also get [testCode.py](/code/testCode.py), and copy the content to code editor [Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) and upload to `Waffle Nano`, run to see the results across the whole library and have a look at the complicity of [joystickShield.py](/code/joystickShield.py).