from joystickShield import Cmd

command=Cmd()
if command.statusA()==1:
    print("A is not pressed.")
else:
    print("A is pressed.")
if command.statusB()==1:
    print("B is not pressed.")
else:
    print("B is pressed.")
if command.statusC()==1:
    print("C is not pressed.")
else:
    print("C is pressed.")
if command.statusD()==1:
    print("D is not pressed.")
else:
    print("D is pressed.")
if command.statusX()==-1:
    print("Stick is on the left.")
elif command.statusX()==0:
    print("Stick is idle horizontally.")
else:
    print("Stick is on the right.")
if command.statusY()==-1:
    print("Stick is down.")
elif command.statusY()==0:
    print("Stick is idle vertically.")
else:
    print("Stick is up.")
print("x="+str(command.exactX())+","+"y="+str(command.exactY()))