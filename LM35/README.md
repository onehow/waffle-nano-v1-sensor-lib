# 温度测量

## 案例展示

一个简单的温度计，检测周围的环境温度，并呈现出来。

<img src="img/IMG20210716152704.jpg" style="zoom:15%;" />

## 物理连线

### 传感器选择

传感器选择如下图的型号为LM35的温度传感模块

<img src="img/IMG20210713193158.jpg" style="zoom:15%;" />

### 传感器连线

传感器与Waffle Nano 之间的接线方式如下表所示

| Waffle Nano | 传感器 |
| :---------- | ------ |
| 3V3         | VCC    |
| IO13        | OUT    |
| GND         | GND    |

## 传感器库使用

可以获取[LM35.py](https://gitee.com/blackwalnutlabs/waffle-nano-v1-sensor-lib/blob/master/LM35/code/LM35.py)，将此库通过[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor)的文件上传到`Waffle Nano`上。

因为该传感器测温库较为简单，所以我们可以直接在主函数中使用以下代码导入此库

```python
import LM35
```

接着，定义一个新的变量接收`temp()`方法传回来的结果

```python
te=LM35.temp()#获取温度
```

关于此库相关细节说明详见代码注释。

## 案例代码复现

可以获取[main.py](https://gitee.com/blackwalnutlabs/waffle-nano-v1-sensor-lib/blob/master/LM35/code/main.py)函数,将其内容复制到[Waffle Makeer](https://wafflenano.blackwalnut.tech/ide/index.html#/editor)编辑器上传输给`Waffle Nano`，以复现此案例。

案例相关细节说明详见代码注释。

