from utime import sleep_ms


# 定义函数
def delay(time):
    sleep_ms(time)


# 定义Board类
class Board:
    # 颜色参数
    background_color = 0x0000
    grid_color = 0xf7f5

    # 方格参数
    screen_length = 240
    grid_number = 24
    grid_length = 8

    # 坐标参数
    start_x = 1
    start_y = 1
    step = grid_length+2

    # 初始化
    def __init__(this, screen):
        # 初始化屏幕
        this.screen = screen
        this.screen.init()
        this.screen.draw_string(40, 230, "Designed by Tatsukana Kinoshita.", size=1)
        delay(2000)
        this.screen.fill(this.background_color)

    # 新建页面
    def new(this):
        this.screen.fill(this.background_color)

    # 平铺点阵
    def tile(this, color):
        x = this.start_x
        y = this.start_y

        for row in range(this.grid_number):
            for column in range(this.grid_number):
                this.point(row, column, color)

    # 填充点阵
    def point(this, row, column, color):
        x = this.start_x+row*this.step
        y = this.start_y+column*this.step
        this.screen.fill_rect(x, y, this.grid_length, this.grid_length, color)

    # 填充边框
    def border(this, width):
        this.screen.fill_rect(0, 0, this.screen_length, width, this.grid_color)
        this.screen.fill_rect(0, 0, width, this.screen_length, this.grid_color)
        this.screen.fill_rect(this.screen_length-width, 0, width, this.screen_length,  this.grid_color)
        this.screen.fill_rect(0, this.screen_length-width,  this.screen_length, width, this.grid_color)

    # 填充文本
    def string(this, x, y, text, size):
        this.screen.draw_string(x, y, text, color=this.grid_color, bg=this.background_color, size=size)
