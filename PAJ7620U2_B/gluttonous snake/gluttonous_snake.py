from utime import sleep_ms
from board import Board
from element import Snake
from board import Board


# 定义函数
def delay(time):
    sleep_ms(time)


# 定义GluttonousSnake类
class GluttonousSnake:
    def __init__(this, screen, gesture, snake_length=3, snake_speed=3):
        this.draw = Board(screen)
        this.s = Snake((0, this.draw.grid_number-1), snake_length, snake_speed)

        this.gesture = gesture

    def start(this):
        this.draw.new()

        this.draw.string(35, 50, "C l o c k w i s e", 2)
        this.draw.string(105, 120, "T o", 2)
        this.draw.string(75, 190, "S t a r t", 2)

    def restart(this):
        this.draw.new()

        this.draw.string(35, 20, "C l o c k w i s e", 2)
        this.draw.string(105, 40, "T o", 2)
        this.draw.string(55, 60, "R e s t a r t", 2)

        this.draw.string(105, 120, "O R", 2)

        this.draw.string(65, 150, "C o u t e r", 2)
        this.draw.string(35, 170, "C l o c k w i s e", 2)
        this.draw.string(105, 190, "T o", 2)
        this.draw.string(85, 210, "E i x t", 2)

    def init(this):
        this.draw.new()
        this.s.new()

        this.draw.border(1)

        for i in this.s.body.element:
            this.draw.point(i[0], i[1], this.s.body_color)

        this.draw.point(this.s.food.coordinate[0], this.s.food.coordinate[1], this.s.food.box_color)

    def go(this, direction):
        result = this.s.turn(direction)

        if result:
            this.draw.point(this.s.get_head()[0], this.s.get_head()[1], this.s.body_color)

            if not this.s.eat(this.s.get_head()):
                this.draw.point(this.s.get_tail()[0], this.s.get_tail()[1], this.draw.background_color)
                this.s.body.dequeue()
            else:
                this.draw.point(this.s.food.coordinate[0], this.s.food.coordinate[1], this.s.food.box_color)

            return True
        else:
            return False

    def play(this):
        timer = 0
        gesture = 0
        result = True

        while True:
            data = this.gesture.get_gesture()  # 获取手势数据

            if data:  # 保存手势数据
                print(data)
                gesture = data

            # 控制蛇移动速度
            if timer < 1000:
                # timer += 1
                timer += 1+(this.s.speed-1)*0.5

            else:
                timer = 0

                if gesture:
                    if gesture == this.gesture.up or gesture == this.gesture.forward:
                        result = this.go(this.s.up)
                    elif gesture == this.gesture.down or gesture == this.gesture.backward:
                        result = this.go(this.s.down)
                    elif gesture == this.gesture.left:
                        result = this.go(this.s.left)
                    elif gesture == this.gesture.right:
                        result = this.go(this.s.right)

                    elif gesture == this.gesture.clockwise:
                        if this.s.direction == this.s.up:
                            result = this.go(this.s.right)
                            result = this.go(this.s.down)
                        elif this.s.direction == this.s.down:
                            result = this.go(this.s.left)
                            result = this.go(this.s.up)
                        elif this.s.direction == this.s.left:
                            result = this.go(this.s.up)
                            result = this.go(this.s.right)
                        elif this.s.direction == this.s.right:
                            result = this.go(this.s.down)
                            result = this.go(this.s.left)
                    elif gesture == this.gesture.counterclockwise:
                        if this.s.direction == this.s.up:
                            result = this.go(this.s.left)
                            result = this.go(this.s.down)
                        elif this.s.direction == this.s.down:
                            result = this.go(this.s.right)
                            result = this.go(this.s.up)
                        elif this.s.direction == this.s.left:
                            result = this.go(this.s.down)
                            result = this.go(this.s.right)
                        elif this.s.direction == this.s.right:
                            result = this.go(this.s.up)
                            result = this.go(this.s.left)

                    # 重置手势
                    gesture = 0

                else:
                    result = this.go(this.s.direction)

                if not result:  # 游戏失败
                    delay(1500)
                    this.draw.new()
                    this.draw.string(50, 50, "G A M E", 4)
                    this.draw.string(50, 150, "O V E R", 4)

                    delay(800)
                    this.draw.new()
                    this.draw.string(50, 40, "G A M E", 4)
                    this.draw.string(50, 130, "O V E R", 4)

                    delay(800)
                    this.draw.new()
                    this.draw.string(50, 30, "G A M E", 4)
                    this.draw.string(50, 130, "O V E R", 4)

                    this.draw.string(50, 185, "Y o u r", 2)
                    this.draw.string(50, 205, "S c o r e", 2)
                    this.draw.string(160, 190, str(this.s.score), 3)

                    return False
