# TOF10120激光测距传感器

## 案例展示

&emsp;&emsp;通过传感器测定屏幕和人脸的距离，如果距离大于50cm，则显示'too far',如果距离小于30cm,显示'too close!',如果距离在两者之间，显示'perfect!'

![输入图片说明](https://images.gitee.com/uploads/images/2021/0716/201859_ba917165_9429392.jpeg "使用界面.jpg")


&emsp;&emsp;后续将改进字体清晰度。此外，使用该工具超过半小时(默认你使用手机与该工具几乎同时开始),将会提醒你休息3分钟，保护视力。
![输入图片说明](https://images.gitee.com/uploads/images/2021/0716/202049_f42dc2f9_9429392.jpeg "休息界面.jpg")

## 物理连接(如下图)

![输入图片说明](https://images.gitee.com/uploads/images/2021/0716/202535_9f538daa_9429392.jpeg "连接情况.jpg")



### 传感器选择

&emsp;&emsp; 传感器选择如图所示的型号为TF10120的激光测距传感器(量程5-180cm)



### 传感器接线

&emsp;&emsp;传感器与Waffle Nano 之间的接线方法可看上图。红色连3.3v电源,黑色连GND，白色连G12 ,黄色连G11(TXD)




## 传感器库使用

&emsp;&emsp;可以获取TOF10120/code/main.py里的tof10120.py,将此库通过[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 的文件上传功能将此库上传到`Waffle Nano`上。

&emsp;&emsp;我们在可以在主函数中使用以下代码导入此库。

```python
from tof10120 import uart_init,convert_data,screen_init 
```

&emsp;&emsp;以上三个函数依次对应 uart初始化(并返回uart对象), 数据处理(返回处理后的数据),屏幕初始化(返回屏幕对象)



&emsp;&emsp;在主函数uart.read()之后，我们通过convert_data处理数据，得到只包含数字的字符串。

```python
x = uart.read()# 读取数据从激光传感器中获取数据
```
&emsp;&emsp;关于此库相关细节说明详见 **代码注释** 

## 案例代码复现

&emsp;&emsp;可以获取TOF10120/code/main.py函数，将其内容复制到[Waffle Maker](https://wafflenano.blackwalnut.tech/ide/index.html#/editor) 编辑器上传输给`Waffle Nano`，以复现此案例。

&emsp;&emsp;案例相关细节说明详见代码注释[输入链接说明](http://)