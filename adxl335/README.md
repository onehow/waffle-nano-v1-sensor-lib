v\#传感器选择
   传感器选择如下图所示的型号为adxl335的加速度模组。
# 加速度模块


#传感器接线
  传感器与Waffle Nano 之间的接线方式如下表所示，且未在下表中显示的引脚均处于悬空不连装态。

#Waffle Nano	传感器
|Waffle Nano | 传感器  |
|--------    |  -- ----|
| 3.3        |   vcc  |
| IO5        |   Xout  |
| IO12       | Yout  |
| IO13       | Zout  |
| GND        |  GND |
## 物理连接

### 传感器选择 



### 传感器接线 

  传感器与Waffle Nano 之间的接线方式如下表所示，且未在下表中显示的引脚均处于悬空不连装态。

| Waffle Nano |      |
| ----------- | ---- |
| 3.3         | VCC  |
| IO5         | xout  |
| IO12        | yout  |
| io13        | zout  |
| GND         | GND  |


#传感器库使用
  可以获取adxl335.py,将此库通过Waffle Maker 的文件上传功能将此库上传到Waffle Nano上。
  我们在可以在主函数中使用以下代码导入此库。

```
import adxl335
```

  在对象构造函数中，我们需要传入一个已经构造好的`ADC`对象。

```
# 此处省略ADC对象的构造过程
from adxl335 import adxl335 
       Xyzread()#读取xyz三轴加速度，返回列表
#案例代码复现
       Xyzread()#读取xyz三轴加速度

## 案例代码复现
 Simple code：打印三轴加速度
>>>while 1:
    >>>acclist=xyzread(x,y,z)